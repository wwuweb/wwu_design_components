<?php
/**
 * @file
 * wwu_design_components.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function wwu_design_components_image_default_styles() {
  $styles = array();

  // Exported image style: faculty_profile_thumbnail.
  $styles['faculty_profile_thumbnail'] = array(
    'label' => 'Faculty Profile Thumbnail',
    'effects' => array(
      4 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 250,
          'height' => 250,
        ),
        'weight' => 1,
      ),
      6 => array(
        'name' => 'image_desaturate',
        'data' => array(),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: front_page_featured.
  $styles['front_page_featured'] = array(
    'label' => 'Front page featured',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 400,
          'height' => 280,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: portfolio_slideshow.
  $styles['portfolio_slideshow'] = array(
    'label' => 'Portfolio Slideshow',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 450,
          'height' => 450,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: student_profile_thumbnail.
  $styles['student_profile_thumbnail'] = array(
    'label' => 'Student Profile Thumbnail',
    'effects' => array(
      3 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 250,
          'height' => 250,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function wwu_design_components_node_info() {
  $items = array(
    'designer_tip' => array(
      'name' => t('Designer Tip'),
      'base' => 'node_content',
      'description' => t('Short designer tips for front page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'faculty' => array(
      'name' => t('Faculty'),
      'base' => 'node_content',
      'description' => t('Faculty member profile.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'featured_content' => array(
      'name' => t('Featured Content'),
      'base' => 'node_content',
      'description' => t('A page with drag-gable paragraph blocks.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'notable_alumni' => array(
      'name' => t('Notable Alumni'),
      'base' => 'node_content',
      'description' => t('Alumni that appear on the "Students" page.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Basic page'),
      'base' => 'node_content',
      'description' => t('Use <em>basic pages</em> for your static content, such as an \'About us\' page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'program' => array(
      'name' => t('Program'),
      'base' => 'node_content',
      'description' => t('A discrete block of content that appears on the programs page. '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'student_profile' => array(
      'name' => t('Student'),
      'base' => 'node_content',
      'description' => t('A collection of a student or faculty\'s projects'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function wwu_design_components_paragraphs_info() {
  $items = array(
    'call_to_action' => array(
      'name' => 'Call to Action',
      'bundle' => 'call_to_action',
      'locked' => '1',
    ),
    'featured_image' => array(
      'name' => 'Featured Image',
      'bundle' => 'featured_image',
      'locked' => '1',
    ),
    'short_post' => array(
      'name' => 'Short Post',
      'bundle' => 'short_post',
      'locked' => '1',
    ),
  );
  return $items;
}
